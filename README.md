## Project Install

In the project folder do:
1. Run command: composer install

## Commands

| Command | Description |
| ------ | ------ |
| ```$ php bin/console company:employee <employeeType>``` | This command allows you to view result of employee work by employeeType(programmer, designer, tester, manager) |
| ```$ php bin/console employee:can <employeeType> <workType>``` | This command allows you to view can an employee do certain actions. |
