<?php

namespace App\Command;

use App\Model\AbstractEmployee;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CompanyEmployeeCommand extends Command
{
    protected $container;
    protected static $defaultName = 'company:employee';
    protected $logger;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('This command allows you to view result of employee work.')
            ->setHelp('This command allows you to view result of employee work by employeeType...')
            ->addArgument('employeeType', InputArgument::REQUIRED, 'The type of employee (programmer, designer, tester, manager)');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $employeeService = $this->container->get('app.service.employee');
        $employeeByType = $employeeService->createEmployeeByString($input->getArgument('employeeType'));
        if ($employeeByType instanceof AbstractEmployee) {
            $output->writeln($employeeByType->work());
        } else {
            $output->writeln('jobType not founded. Try (programmer, designer, tester, manager)');
        }
    }
}
