<?php

namespace App\Command;

use App\Model\AbstractEmployee;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EmployeeCanCommand extends Command
{
    protected $container;
    protected static $defaultName = 'employee:can';
    protected $logger;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('This command allows you to view can an employee do certain actions.')
            ->setHelp('This command allows you to view can an employee do certain actions.')
            ->addArgument('employeeType', InputArgument::REQUIRED, 'The employee type of employee (programmer, designer, tester, manager)')
            ->addArgument('workType', InputArgument::REQUIRED, 'The work type of employee');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $employeeService = $this->container->get('app.service.employee');
        $employeeByType = $employeeService->createEmployeeByString($input->getArgument('employeeType'));
        if ($employeeByType instanceof AbstractEmployee) {
            $output->writeln($employeeByType->isCanDoWork($input->getArgument('workType'))?'true':'false');
        } else {
            $output->writeln('jobType not founded. Try (programmer, designer, tester, manager)');
        }
    }
}
