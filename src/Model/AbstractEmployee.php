<?php

namespace App\Model;

/**
 * Abstract AbstractEmployee
 */
abstract class AbstractEmployee
{
    /**
     * @return array
     */
    abstract public function work(): array;

    /**
     * @param string $methodName
     *
     * @return bool
     */
    public function isCanDoWork(string $methodName): bool
    {
        if (method_exists($this, $methodName)) {
            return true;
        } else {
            return false;
        }
    }
}
