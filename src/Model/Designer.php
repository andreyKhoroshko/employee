<?php

namespace App\Model;

class Designer extends AbstractEmployee
{

    /**
     * @return string
     */
    private function draw(): string
    {
        return '- drawing';
    }


    /**
     * @return string
     */
    private function communicateWithManager(): string
    {
        return '- communication with manager';
    }

    /**
     * @return array
     */
    public function work(): array
    {
        $workList[] = $this->draw();
        $workList[] = $this->communicateWithManager();
        return $workList;
    }
}
