<?php

namespace App\Model;

class Manager extends AbstractEmployee
{

    /**
     * @return string
     */
    private function setTasks(): string
    {
        return '- setting tasks';
    }

    /**
     * @return array
     */
    public function work(): array
    {
        $workList[] = $this->setTasks();
        return $workList;
    }
}
