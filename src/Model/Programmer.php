<?php

namespace App\Model;

class Programmer extends AbstractEmployee
{

    /**
     * @return string
     */
    private function writeCode(): string
    {
        return '- code writing';
    }

    /**
     * @return string
     */
    private function testCode(): string
    {
        return '- code testing';
    }

    /**
     * @return string
     */
    private function communicateWithManager(): string
    {
        return '- communication with manager';
    }

    /**
     * @return array
     */
    public function work(): array
    {
        $workList[] = $this->writeCode();
        $workList[] = $this->testCode();
        $workList[] = $this->communicateWithManager();
        return $workList;
    }
}
