<?php

namespace App\Model;

class Tester extends AbstractEmployee
{

    /**
     * @return string
     */
    private function setTasks(): string
    {
        return '- setting tasks';
    }

    /**
     * @return string
     */
    private function testCode(): string
    {
        return '- code testing';
    }

    /**
     * @return string
     */
    private function communicateWithManager(): string
    {
        return '- communication with manager';
    }

    /**
     * @return array
     */
    public function work(): array
    {
        $workList[] = $this->testCode();
        $workList[] = $this->communicateWithManager();
        $workList[] = $this->setTasks();
        return $workList;
    }
}
