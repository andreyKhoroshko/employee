<?php

namespace App\Service;

use App\Model\AbstractEmployee;
use App\Model\Designer;
use App\Model\Manager;
use App\Model\Programmer;
use App\Model\Tester;

class EmployeeService
{
    /**
     * @param string $employeeType
     *
     * @return null|AbstractEmployee
     */
    public function createEmployeeByString(string $employeeType): ?AbstractEmployee
    {
        switch (strtolower(trim($employeeType))) {
            case 'programmer':
                return new Programmer();
                break;
            case 'designer':
                return new Designer();
                break;
            case  'tester':
                return new Tester();
                break;
            case  'manager':
                return new Manager();
                break;
            default:
                return null;
        }
    }
}
